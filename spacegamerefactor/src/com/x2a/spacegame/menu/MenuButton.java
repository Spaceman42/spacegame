package com.x2a.spacegame.menu;

import com.x2a.game.ScaledSprite;
import com.x2a.game.Sprite;
import com.x2a.game.gui.Button;
import com.x2a.graphics.render.RenderAPI;

import java.awt.*;

/**
 * Created by Ethan on 2/5/2015.
 */
public abstract class MenuButton extends Button{

    public MenuButton(Sprite sprite, int[] listeningButtons, float yPosition) {
        super(new ScaledSprite(sprite, 800, 240), listeningButtons);

        setPosition(-550f, yPosition);
        //setScale(sprite.getWidth()/1300.0f);
    }


    @Override
    public void draw(RenderAPI renderAPI) {
        setDepth(getSprite().getDepth() - 1);
        if (isMouseOver()) {
            Color color = new Color(0, 255, 0, 155);
            renderAPI.drawRectangle(getAbsoluteTransform(), color, true, getSprite().getWidth(), getSprite().getHeight());
        }

    }

}
