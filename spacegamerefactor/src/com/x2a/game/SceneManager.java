package com.x2a.game;

import com.x2a.scene.Scene;

import java.util.Collection;
import java.util.Stack;
import java.util.function.Consumer;

/**
 * Created by Ethan on 2/4/2015.
 */
public class SceneManager {

    private Stack<Scene> sceneStack;

    public SceneManager() {
        sceneStack = new Stack<Scene>();
    }


    public Scene getCurrentScene() {
        return sceneStack.peek();
    }

    public Scene popScene() {
        sceneStack.peek().deactivate();
        Scene old = sceneStack.pop();
        sceneStack.peek().activate();
        return old;
    }

    public void pushScene(Scene scene) {
        if (!sceneStack.empty() && sceneStack.peek() != null) {
            sceneStack.peek().deactivate();
        }
        sceneStack.push(scene);
        sceneStack.peek().activate();
    }

    public Collection<Scene> getAllScenes() {
        return sceneStack;
    }

    public void forEach(Consumer<Scene> action) {
        sceneStack.forEach(action);
    }
}
