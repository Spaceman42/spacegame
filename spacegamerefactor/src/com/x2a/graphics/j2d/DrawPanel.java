package com.x2a.graphics.j2d;

import com.x2a.App;
import com.x2a.input.SafeInputUtil;
import com.x2a.scene.GameObject;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by 40501 on 1/26/2015.
 */
public class DrawPanel extends JPanel{
    private J2dRenderAPI renderAPI;

    private GameObject rootNode;

    private final float scaleFactor;

    public DrawPanel(int x, int y) {
        setFocusable(true);
        requestFocusInWindow();

        addMouseListener(SafeInputUtil.getInstance());
        addMouseMotionListener(SafeInputUtil.getInstance());
        addMouseWheelListener(SafeInputUtil.getInstance());
        addKeyListener(SafeInputUtil.getInstance());

        scaleFactor = x/App.GAME_SIZE_X;

        renderAPI = new J2dRenderAPI();

        setPreferredSize(new Dimension(x, y));
    }

    public void update(GameObject rootNode) {
        this.rootNode = rootNode;
        paintImmediately(0, 0, getWidth(), getHeight());
        //repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.clearRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.BLACK);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.translate(getWidth()/2, getHeight()/2);
        g2.scale(scaleFactor, scaleFactor);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        AffineTransform originTransform = g2.getTransform();

        renderAPI.setGraphics2D(g2);

        rootNode.doNodeTask((GameObject n) -> n.computeTransform());

        Set<GameObject> nodes = sortNodes(rootNode.getAllNodes());

        for (GameObject n : nodes) {
            n.draw(renderAPI);
            g2.setTransform(originTransform);
        }

        //renderAPI.finishFrame();
        //rootNode = null;
    }

    private Set<GameObject> sortNodes(Collection<GameObject> nodes) {
        Set<GameObject> output =  new TreeSet<GameObject>(new Comparator<GameObject>() {
            @Override
            public int compare(GameObject o1, GameObject o2) {
                float val = o2.getDepth() - o1.getDepth(); //Higher depth draws first

                if (val == 0) {
                    val = 1;
                }

                return (int)(val/Math.abs(val));
            }
        });
        output.addAll(nodes);
        return output;
    }

    public J2dRenderAPI getRenderAPI() {
        return renderAPI;
    }
}
