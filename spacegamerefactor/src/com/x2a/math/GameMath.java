package com.x2a.math;

import com.sun.deploy.util.ArrayUtil;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ethan on 2/4/2015.
 */
public class GameMath {
    private GameMath() {}


    public static boolean pointInsideBounds(float px, float py, float x1, float y1, float x2, float y2) {
        if (x1 < x2) {
            if (px < x1 || px > x2) {
                return false;
            }
        } else if (x1 > x2) {
            if (px < x2 || px > x1) {
                return false;
            }
        } else {
            return false;
        }

        if (y1 < y2) {
            if (py < y1 || py > y2) {
                return false;
            }
        } else if (y1 > y2) {
            if (py > y1 || py < y2) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    public static float getRotation(AffineTransform t) {
        double[] mat = new double[6];
        t.getMatrix(mat);

        double[] row0 = {mat[0], mat[1], mat[2]};
        double[] row1 ={mat[3], mat[4], mat[5]};

        for (int i = 0; i < row0.length; i++) {
            row0[i]/=t.getScaleX();
        }

        for (int i = 0; i < row1.length; i++) {
            row1[i]/=t.getScaleY();
        }

        return (float)Math.acos(row0[0]);
    }
}
