package com.x2a.physics;

/**
 * Created by Ethan on 2/26/2015.
 */
public class BodyProperties {
    public float density;
    public float restitution;
    public float friction;


    public BodyProperties(float density, float restitution, float friction) {
        this.density = density;
        this.restitution = restitution;
        this.friction = friction;
    }
}
