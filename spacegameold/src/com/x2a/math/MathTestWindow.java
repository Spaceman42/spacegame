package com.x2a.math;

import com.x2a.math.sat.ConvexHull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by David on 1/25/2015.
 */
public class MathTestWindow extends JFrame {

    private JPanel p;
    private List<Vector2> points;
    private ConvexHull convexHull;
    //private Vector2 averagePoint;
    //private Vector2 currentPoint;
    //private Vector2 nextPoint;

    public MathTestWindow() {
        p = new JPanel();
        p.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                points = randomPoints(4, 50, 50);
                convexHull = ConvexHull.createConvexHull(points);

                repaint();
                if (convexHull.failed)
                    JOptionPane.showMessageDialog(p, "Failed");

                //currentPoint = new Vector2(e.getX() - 300 + 4, e.getY() - 200 + 25);
                //JOptionPane.showMessageDialog(p, ConvexHull.crossProductZ(averagePoint, farthestPoint, currentPoint));
            }
        });
        setSize(600, 400);
        getContentPane().add(p);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        points = randomPoints(4, 50, 50);
        convexHull = ConvexHull.createConvexHull(points);
    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.translate(300, 200);
        for (Vector2 v : points)
            g2.fillOval((int) v.x - 2, (int) v.y - 2, 4, 4);
        g2.setColor(Color.ORANGE);

        for (int i = 0; i < convexHull.points.size(); i++) {
            g2.fillOval((int) convexHull.points.get(i).x - 4, (int) convexHull.points.get(i).y - 4, 8, 8);
            g2.drawLine((int) convexHull.points.get(i).x, (int) convexHull.points.get(i).y,
                    (int) convexHull.points.get(i != 0 ? i - 1 : convexHull.points.size() - 1).x,
                    (int) convexHull.points.get(i != 0 ? i - 1 : convexHull.points.size() - 1).y);
        }

        Vector2 point = ConvexHull.averagePoint(points);
        g2.setColor(Color.BLUE);
        g2.fillOval((int) point.x - 4, (int) point.y - 4, 8, 8);

        point = ConvexHull.farthestPoint(points);
        g2.setColor(Color.RED);
        g2.fillOval((int) point.x - 4, (int) point.y - 4, 8, 8);

//        g2.setColor(Color.RED);
//        g2.fillOval((int) averagePoint.x, (int) averagePoint.y, 8, 8);
//        g2.setColor(Color.GREEN);
//        g2.fillOval((int) nextPoint.x, (int) nextPoint.y, 8, 8);
//        if (currentPoint != null) {
//            g2.setColor(Color.YELLOW);
//            g2.fillOval((int) currentPoint.x, (int) currentPoint.y, 8, 8);
//        }
    }

    private List<Vector2> randomPoints(int num, float stdX, float stdY) {
        List<Vector2> points = new ArrayList();
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            points.add(new Vector2((float) r.nextGaussian() * stdX, (float) r.nextGaussian() * stdX));
        }
        return points;
    }

    public static void main(String[] args) {
        MathTestWindow mtw = new MathTestWindow();
        mtw.setVisible(true);
    }
}
