package com.x2a.graphics.render;

import com.x2a.scene.GameObject;

/**
 * Created by 40501 on 1/26/2015.
 */
public interface Window {
    public RenderAPI getRenderAPI();
    public void update(GameObject rootNode);
    public void create();

    public int getWidth();
    public int getHeight();

    public void close();

    public void mouseExited();
    public void mouseEntered();
}
