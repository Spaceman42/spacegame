package com.x2a.physics;

import com.x2a.math.Vector2;
import org.jbox2d.common.Vec2;

/**
 * Created by Ethan on 2/26/2015.
 */
public class MathUtil {
    private MathUtil() {}


    public static Vec2 toVec2(Vector2 vec) {
        return new Vec2(vec.x, vec.y);
    }

    public static Vector2 convertUnits(Vector2 pixelPosition, int pixelsPerUnit) {
        return new Vector2(pixelPosition.x/pixelsPerUnit, pixelPosition.y/pixelsPerUnit);
    }

    public static Vector2 toVector2(Vec2 vec) {
        return new Vector2(vec.x, vec.y);
    }

    public static Vector2 convertToPixelUnits(Vector2 physPosition, int pixelsPerUnit) {
        return new Vector2(physPosition.x * pixelsPerUnit, physPosition.y * pixelsPerUnit);
    }
}
