package com.x2a;

import com.x2a.engine.GameLoop;
import com.x2a.engine.VariableGameLoop;
import com.x2a.game.Game;
import com.x2a.graphics.j2d.J2dWindow;
import com.x2a.graphics.render.RenderAPI;
import com.x2a.graphics.render.Window;
import com.x2a.input.*;
import com.x2a.scene.GameObject;
import com.x2a.settings.DefualtSettingsTable;
import com.x2a.settings.Settings;
import com.x2a.spacegame.SpaceGame;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

/**
 * Created by Ethan on 1/25/2015.
 */
public class App {

    public static final float GAME_SIZE_X = 2440.0f;
    public static final float GAME_SIZE_Y = 1440.0f;


    private static Settings settings;

    private static final String SETTINGS_LOCATION = "settings.cfg";

    private static Window window;

    private static final float frameTime = 16.666f;

    private static RenderAPI renderAPI;

    private static GameLoop gameLoop;

    private static int mouseX;
    private static int mouseY;

    private static int rawMouseX;
    private static int rawMouseY;

    private static InputListenerContainer inputListenerContainer = new InputListenerContainer();

    public static void main(String[] args) {
        try {
            loadSettings();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        window = new J2dWindow();
        renderAPI = window.getRenderAPI();

        App app = new App(new SpaceGame());
        app.run();
    }

    private static void loadSettings() throws IOException {
        File settingsFile = new File(SETTINGS_LOCATION);
        if (!settingsFile.exists()) {
            settings = new Settings(new DefualtSettingsTable());
            settings.writeSettings(SETTINGS_LOCATION);
        } else {
            settings = Settings.loadSettings(SETTINGS_LOCATION);
        }
    }
    private static Game game;

    public App(Game game) {
        this.game = game;
        game.initSceneGraph();

        SafeInputUtil.getInstance().registerKeyEventListener(new KeyEventListener() {
            @Override
            public void onKeyEvent(KeyEventData data) {
                if (data.getEventType() == KeyEventType.KEY_PRESSED) {
                    if (data.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        System.exit(0);
                    }
                }
                inputListenerContainer.onKeyEvent(data);
            }
        });
        SafeInputUtil.getInstance().registerMouseEventListener(new MouseEventListener() {
            @Override
            public void onMouseEvent(MouseEventData data) {
                if (data.getEventType() == MouseEventType.MOUSE_EXITED) {
                    window.mouseExited();
                } else if (data.getEventType() == MouseEventType.MOUSE_ENTERED) {
                    window.mouseEntered();
                } else if (data.getEventType() == MouseEventType.MOUSE_MOVED) {
                    mouseX = (int) data.getPosition().x;
                    mouseY = (int) data.getPosition().y;

                    rawMouseX = (int) data.getScreenPosition().x;
                    rawMouseY = (int) data.getScreenPosition().y;
                } else {
                    inputListenerContainer.onMouseEvent(data);
                }
            }
        });
        gameLoop = new VariableGameLoop(game, window, 16.66667);
    }

    public void run() {
        window.update(game.getRootNode());

        window.create();

        gameLoop.run();
        System.out.println("Run finished");
    }

    public static Settings settings() {
        return settings;
    }

    public static RenderAPI renderAPI() {
        return renderAPI;
    }

    public static boolean isKeyDown(char key) {
        return SafeInputUtil.getInstance().isKeyDown(key);
    }

    public static boolean isButtonDown(int button) {
        return SafeInputUtil.getInstance().isButtonDown(button);
    }

    public static int getMouseX() {
        return (int)(mouseX*(GAME_SIZE_X/window.getWidth()));
    }

    public static int getMouseY() {
        return (int)(mouseY *(GAME_SIZE_X/window.getWidth()));
    }

    public static int getRawMouseX() {
        return rawMouseX;
    }

    public static int getRawMouseY() {
        return rawMouseY;
    }

    public static void registerInputNode(GameObject n) {
        inputListenerContainer.registerNode(n);
    }

    public static Game game() {
        return game;
    }

    public static void endGame() {
        gameLoop.stop();
    }

}
