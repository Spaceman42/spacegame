package com.x2a.scene;

import com.x2a.math.Vector2;

/**
 * Created by 40501 on 1/27/2015.
 */
public abstract class Script extends GameObject {

    @Override
    public void setPosition(Vector2 position) {
        getParent().setPosition(position);
    }

    @Override
    public void setRotation(float rotation) {
        getParent().setRotation(rotation);
    }

    @Override
    public void setScale(float scale) {
        (getParent()).setScale(scale);
    }

    public void setScale(float scaleX, float scaleY) {
        getParent().setScale(scaleX, scaleY);
    }

    @Override
    public void setDepth(int depth) {
        (getParent()).setDepth(depth);
    }

    public int getDepth(int depth) {
        return getParent().getDepth();
    }

    public float getScaleX() {
        return getParent().getScaleX();
    }

    public float getScaleY() {
        return getParent().getScaleY();
    }

    public Vector2 getPosition() {
        return getParent().getPosition();
    }

    public float getRotation() {
        return getParent().getRotation();
    }

    public void rotate(float amount) {
        getParent().rotate(amount);
    }

    @Override
    public void translate(Vector2 vec) {
        getParent().translate(vec);
    }

    public abstract void onUpdate(float timeElapsed);

    @Override
    public void update(float timeElapsed) {
        super.update(timeElapsed);
        onUpdate(timeElapsed);
    }

}
