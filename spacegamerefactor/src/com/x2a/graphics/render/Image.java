package com.x2a.graphics.render;

import java.awt.*;

/**
 * Created by 40501 on 1/26/2015.
 */
public interface Image {
    public Color getColor(int x, int y);
    public void setColor(Color color, int x, int y);
    public int getRegionHeight();
    public int getRegionWidth();
    public int getNumberOfRegions();
    public int getWidth();
    public int getHeight();
    public int getNumRegionX();
    public int getNumRegionY();
}
