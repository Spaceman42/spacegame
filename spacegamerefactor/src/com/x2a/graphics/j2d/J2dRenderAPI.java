package com.x2a.graphics.j2d;

import com.x2a.graphics.render.Image;
import com.x2a.graphics.render.RenderAPI;
import com.x2a.math.Vector2;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;

/**
 * Created by 40501 on 1/26/2015.
 */
public class J2dRenderAPI implements RenderAPI{

    private Graphics2D g2;
    private boolean inFrame;

    void setGraphics2D(Graphics2D g2) {
        this.g2 = g2;
        inFrame = true;
    }

    void finishFrame() {
        g2 = null;
        inFrame = false;
    }

    private void doInitTranslation(Graphics2D g2, AffineTransform t, int width, int height) {
        g2.transform(t);
        g2.translate(-width/2, -height/2);
    }

    @Override
    public void drawImageRegion(AffineTransform transform, int region, Image image) {
        J2dImage jImage = (J2dImage) image;

        int regionStartX = region % image.getNumRegionX();
        int regionStartY = region / image.getNumRegionX();

        doInitTranslation(g2, transform, image.getRegionWidth(), image.getRegionHeight());

        g2.drawImage(jImage.getSubImage(regionStartX, regionStartY), 0, 0, null);
    }

    @Override
    public void drawRectangle(AffineTransform transform, Color color, boolean fill, int width, int height) {
        doInitTranslation(g2, transform, width, height);
        g2.setColor(color);
        if (fill) {
            g2.fillRect(0, 0, width, height);
        } else {
            g2.drawRect(0, 0, width, height);
        }
    }

    @Override
    public void drawOval(AffineTransform transform, Color color, boolean fill, int width, int height) {
        doInitTranslation(g2, transform, width, height);
        g2.setColor(color);
        if (fill) {
            g2.fillOval(0, 0, width, height);
        } else {
            g2.drawOval(0, 0, width, height);
        }
    }

    @Override
    public void drawLine(Color color, Vector2 p1, Vector2 p2) {

    }

    @Override
    public Image loadImage(String location, int regionWidth, int regionHeight) {
        try {
            return J2dImage.loadImage(location, regionWidth, regionHeight);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Image loadImage(int[] data, int width, int height, int regionWidth, int regionHeight) {
        return null;
    }

    @Override
    public Image loadImage(Color[] data, int width, int height, int regionWidth, int regionHeight) {
        return null;
    }
}

