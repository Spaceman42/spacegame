package com.x2a.physics.shape;

import com.x2a.math.GameMath;
import com.x2a.math.Vector2;
import com.x2a.physics.MathUtil;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.collision.shapes.ShapeType;
import org.jbox2d.common.Rot;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Created by Ethan on 2/26/2015.
 */
public class RectangleShape implements PhysicsShape{
    private float width;
    private float height;

    public RectangleShape(float width, float height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public Shape getCollisionShape(int pixelsPerUnit) {
        PolygonShape ps = new PolygonShape();
        ps.setAsBox(width/pixelsPerUnit, height/pixelsPerUnit);
        return ps;
    }

    @Override
    public boolean testPoint(Vector2 p, AffineTransform t) {
        PolygonShape ps = new PolygonShape();
        ps.setAsBox(width*(float)t.getScaleX(), height*(float)t.getScaleY());
        Vec2 trans = new Vec2((float)t.getTranslateX(), (float)t.getTranslateY());
        Transform transform = new Transform(trans, new Rot(GameMath.getRotation(t)));
        return ps.testPoint(transform, MathUtil.toVec2(p));
    }
}
