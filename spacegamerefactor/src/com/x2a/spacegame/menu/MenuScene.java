package com.x2a.spacegame.menu;

import com.x2a.App;
import com.x2a.game.Sprite;
import com.x2a.game.util.BasicControllerScript;
import com.x2a.game.util.RotateControllerScript;
import com.x2a.scene.Camera;
import com.x2a.scene.Scene;
import com.x2a.scene.world.World;

/**
 * Created by 40501 on 2/5/2015.
 */
public class MenuScene extends Scene {
    Camera camera;

    public MenuScene() {
        camera = new Camera();
        camera.addChild(new RotateControllerScript('w', 'a', 's', 'd', 100f, 0.1f));
        getRootNode().addChild(camera);
        Sprite background = new Sprite("res/images/backgrounds/MainMenu Background.png", 2560, 1440);
        background.setDepth(1000);
        background.setScale((float) App.GAME_SIZE_X/2560.0f);
        getRootNode().addChild(background);
        camera.addChild(new MenuPlayButton(-150f));
        camera.addChild(new MenuExitButton(150f));
    }

    @Override
    public World getWorld() {
        return null;
    }


}
