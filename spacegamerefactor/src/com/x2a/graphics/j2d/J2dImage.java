package com.x2a.graphics.j2d;

import com.x2a.graphics.render.Image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by 40501 on 1/28/2015.
 */
public class J2dImage implements Image {

    private BufferedImage image;
    private BufferedImage[][] subImages;
    private int regionHeight;
    private int regionWidth;


    public J2dImage(Color[][] color, int width, int height, int regionWidth, int regionHeight) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.regionHeight = regionHeight;
        this.regionWidth = regionWidth;

        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                image.setRGB(x, y, color[x][y].getRGB());
            }
        }
    }

    public J2dImage(BufferedImage image) {
        this(image, image.getWidth(), image.getHeight());
    }

    public J2dImage(BufferedImage image, int regionWidth, int regionHeight) {
        this.image = image;
        this.regionHeight = regionHeight;
        this.regionWidth = regionWidth;
        subImages = new BufferedImage[getNumRegionX()][getNumRegionY()];

        for (int x = 0; x < getNumRegionX(); x++) {
            for (int y = 0; y <getNumRegionY(); y++) {
                subImages[x][y] = image.getSubimage(x*getRegionWidth(), y*getRegionHeight(), getRegionWidth(), getRegionHeight());
            }
        }
    }

    public static J2dImage loadImage(String location, int regionWidth, int regionHeight) throws IOException {
        BufferedImage image = J2dImageCache.getImage(location);
        return new J2dImage(image, regionWidth, regionHeight);
    }

    @Override
    public Color getColor(int x, int y) {
        return new Color(image.getRGB(x, y));
    }

    @Override
    public void setColor(Color color, int x, int y) {
        image.setRGB(x, y, color.getRGB());
    }

    @Override
    public int getRegionHeight() {
        return regionHeight;
    }

    @Override
    public int getRegionWidth() {
        return regionWidth;
    }

    @Override
    public int getNumberOfRegions() {
        return (getWidth()/regionWidth)*(getHeight()/regionHeight);
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getNumRegionX() {
        return (getWidth()/regionWidth);
    }

    @Override
    public int getNumRegionY() {
        return (getHeight()/regionHeight);
    }

    public BufferedImage getImage() {
        return image;
    }

    public BufferedImage getSubImage(int x, int y) {
        return subImages[x][y];
    }
}
