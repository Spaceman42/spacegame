package com.x2a.settings;

import com.x2a.utils.FileUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 40501 on 1/23/2015.
 */
public class Settings {

    static final char TYPE_FLOAT = 'f';
    static final char TYPE_INT = 'i';
    static final char TYPE_BOOLEAN = 'b';
    static final char TYPE_STRING = 's';
    private static final Pattern SPLITTER = Pattern.compile("\"([^\"]*)\"");

    private Map<String, SettingsNode> settingsMap;

    private Settings(Map<String, SettingsNode> settingsMap) {
        this.settingsMap = settingsMap;
    }

    public Settings(SettingsTable presets) {
        this.settingsMap = presets.getSettingsMap();
    }

    public float getFloat(String node, String name) {
        return settingsMap.get(node.toLowerCase()).getFloat(name.toLowerCase());
    }

    public int getInt(String node, String name) {
        return settingsMap.get(node.toLowerCase()).getInt(name.toLowerCase());
    }

    public void setInt(String node, String name, int value) {
        settingsMap.get(node.toLowerCase()).putInt(name, value);
    }

    public boolean getBoolean(String node, String name) {
        return settingsMap.get(node.toLowerCase()).getBoolean(name.toLowerCase());
    }

    public String getString(String node, String name) {
        return settingsMap.get(node.toLowerCase()).getString(name.toLowerCase());
    }

    public void writeSettings(String fileLocation) throws IOException {
        File file = new File(fileLocation);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();

        StringBuilder data = new StringBuilder();

        for (SettingsNode node: settingsMap.values()) {
            String nodeName = node.getName();
            data.append("[" + nodeName + "]" + "\n");
            data.append(node.writeNode());
        }

        FileUtil.writeFile(file, data.toString());
    }

    public static Settings loadSettings(String fileLocation) throws IOException {
        List<String> data = FileUtil.loadFileLines(new File(fileLocation));

        Map<String, SettingsNode> settingsTable = new HashMap<String, SettingsNode>();
        SettingsNode currentNode = null;
        for (String line : data) {
            line.trim();
            if (line.charAt(0) == '[') {
                String nodeName = line.substring(1, line.length()-1).toLowerCase();
                currentNode = new SettingsNode(nodeName);
                settingsTable.put(nodeName, currentNode);
            } else if (currentNode != null) {
                analyseLine(currentNode, line);
            }

        }
        return new Settings(settingsTable);
    }

    private static void analyseLine(SettingsNode currentNode, String line) {
        if (line.length() > 0) {
            char type = line.charAt(0);
            String[] values = seperateLine(line.substring(1));
            String name = values[0];
            String value = values[1];
            if (type == TYPE_FLOAT) {
                currentNode.putFloat(name, Float.parseFloat(value));
            } else if (type == TYPE_BOOLEAN) {
                currentNode.putBoolean(name, Boolean.parseBoolean(value));
            } else if (type == TYPE_INT) {
                currentNode.putInt(name, Integer.parseInt(value));
            } else if (type == TYPE_STRING) {
                currentNode.putString(name, value);
            }
        }
    }

    private static String[] seperateLine(String line) {
        Matcher m = SPLITTER.matcher(line);
        m.find();
        String value = m.group();
        value = value.substring(1, value.length() - 1);
        String name = line.split("=")[0].trim();
        return new String[]{name, value};
    }

}
