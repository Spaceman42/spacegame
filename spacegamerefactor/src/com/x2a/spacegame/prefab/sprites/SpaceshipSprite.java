package com.x2a.spacegame.prefab.sprites;

import com.x2a.game.AnimatedSprite;
import com.x2a.game.Animation;

/**
 * Created by 40501 on 2/4/2015.
 */
public class SpaceshipSprite extends AnimatedSprite{

    private static final Animation MOVE_ANIMATION = new Animation(new int[]{0, 1, 2}, new float[]{130, 130, 130});
    private static final String IMAGE_LOCATION = "res/images/sprites/SpaceshipAn.png";

    private static final int REGION_WIDTH = 104;
    private static final int REGION_HEIGHT = 60;

    public static final int MOVE_ANIMATION_NUM = 0;

    public SpaceshipSprite() {
        super(IMAGE_LOCATION, REGION_WIDTH, REGION_HEIGHT, MOVE_ANIMATION);
    }
}
