package com.x2a.physics;

import com.x2a.math.Vector2;
import com.x2a.physics.shape.PhysicsShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;

/**
 * Created by Ethan on 2/26/2015.
 */
public class PhysicsBody {

    private Body body;
    private PhysicsWorld world;

    public PhysicsBody(PhysicsShape shape, BodyProperties properties, Vector2 position, PhysicsBodyType type, PhysicsWorld world) {
        this.world = world;
        BodyDef bd = new BodyDef();
        Vector2 worldPos = MathUtil.convertUnits(position, world.getPixelsPerUnit());
        bd.type = type.getBodyType();
        bd.position.set(worldPos.x, worldPos.y);

        FixtureDef fd = new FixtureDef();
        fd.shape = shape.getCollisionShape(world.getPixelsPerUnit());
        fd.density = properties.density;
        fd.friction = properties.friction;
        fd.restitution = properties.restitution;

        body = world.getWorld().createBody(bd);
        body.createFixture(fd);
        body.getTransform();
    }

    protected Vec2 getTruePosition() {
        return body.getPosition();
    }

    protected void setTruePosition(Vec2 vec) {
        body.setTransform(vec, body.getAngle());
    }

    public void setPosition(Vector2 position) {
        setTruePosition(MathUtil.toVec2(MathUtil.convertUnits(position, world.getPixelsPerUnit())));
    }

    public float getRotation() {
        return body.getAngle();
    }

    public void setRotation(float angle) {
        body.setTransform(body.getPosition(), angle);
    }

    protected Vec2 getTrueVelocity() {
        return body.getLinearVelocity();
    }

    public Vector2 getVelocity() {
        return MathUtil.convertToPixelUnits(MathUtil.toVector2(getTrueVelocity()),world.getPixelsPerUnit());
    }

    public Vector2 getPosition() {
        return MathUtil.convertToPixelUnits(MathUtil.toVector2(getTruePosition()), world.getPixelsPerUnit());
    }

    public void destroy() {
        world.getWorld().destroyBody(body);
    }

    public void applyForce(Vector2 position, Vector2 force) {
        Vec2 pos = MathUtil.toVec2(MathUtil.convertUnits(position, world.getPixelsPerUnit()));
        Vec2 force2 = MathUtil.toVec2(MathUtil.convertUnits(force, world.getPixelsPerUnit()));
        System.out.println("Force applied. " + force2);
        //body.applyForce(force2, pos);
        body.applyLinearImpulse(force2, pos);
        System.out.println("Location: " + getTruePosition());
    }

    protected Body getBody() {
        return body;
    }

    public void applyAngularForce(float v) {
        body.applyAngularImpulse(v/world.getPixelsPerUnit());
    }
}
