package com.x2a.physics.shape;

import com.x2a.math.GameMath;
import com.x2a.math.Vector2;
import com.x2a.physics.MathUtil;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Rot;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;

import java.awt.geom.AffineTransform;

/**
 * Created by Ethan on 2/26/2015.
 */
public class CircleShape implements PhysicsShape{
    private float radius;

    public CircleShape(float radius) {
        this.radius = radius;
    }


    @Override
    public Shape getCollisionShape(int pixelsPerUnit) {
        org.jbox2d.collision.shapes.CircleShape cs = new org.jbox2d.collision.shapes.CircleShape();
        cs.m_radius = radius/pixelsPerUnit;
        return cs;
    }

    @Override
    public boolean testPoint(Vector2 p, AffineTransform t) {
        org.jbox2d.collision.shapes.CircleShape cs = new org.jbox2d.collision.shapes.CircleShape();
        cs.m_radius = radius*(float)t.getScaleX();
        Vec2 trans = new Vec2((float)t.getTranslateX(), (float)t.getTranslateY());
        Transform transform = new Transform(trans, new Rot(GameMath.getRotation(t)));
        return cs.testPoint(transform, MathUtil.toVec2(p));
    }
}
