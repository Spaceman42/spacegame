package com.x2a.scene.world;

import com.x2a.scene.GameObject;

/**
 * Created by Ethan on 2/22/2015.
 */
public interface World {
    public void update(float timeElapsed);
}
