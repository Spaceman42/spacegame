package com.x2a.game;

import com.x2a.scene.GameObject;

/**
 * Created by 40501 on 2/27/2015.
 */
public class ScaledSprite extends GameObject{

    private float width;
    private float height;

    private float spriteW;
    private float spriteH;

    private Sprite sprite;

    public ScaledSprite(Sprite sprite, float width, float height) {
        this.width = width;
        this.height = height;

        spriteW = sprite.getWidth();
        spriteH = sprite.getHeight();

        float scaleX = width/spriteW;
        float scaleY = height/spriteH;

        setScale(scaleX, scaleY);

        addChild(sprite);

        this.sprite =sprite;
    }

    public void setWidth(float width) {
        this.width = width;
        setScaleX(width/spriteW);
    }

    public void setHeight(float height) {
        this.height = height;
        setScaleY(height/spriteH);
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getSpriteW() {
        return spriteW;
    }

    public float getSpriteH() {
        return spriteH;
    }

    public Sprite getSprite() {
        return sprite;
    }

    @Override
    protected void onUpdate(float timeElapsed) {}

    @Override
    protected void onActivate() {}

    @Override
    protected void onDeactivate() {}
}
