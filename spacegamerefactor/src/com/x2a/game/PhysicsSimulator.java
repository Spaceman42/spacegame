package com.x2a.game;

import com.x2a.math.Vector2;
import com.x2a.physics.BodyProperties;
import com.x2a.physics.PhysicsBody;
import com.x2a.physics.PhysicsBodyType;
import com.x2a.physics.PhysicsWorld;
import com.x2a.physics.shape.PhysicsShape;
import com.x2a.scene.Script;

/**
 * Created by 40501 on 3/5/2015.
 */
public class PhysicsSimulator extends Script {

    private PhysicsBody body;

    public PhysicsSimulator(PhysicsShape shape, float densitt, float restitution, float friction, Vector2 position, PhysicsWorld world) {
        BodyProperties bp = new BodyProperties(densitt, restitution, friction);

        body = new PhysicsBody(shape, bp, position, PhysicsBodyType.DYNAMIC, world);
    }

    public void applyForce(Vector2 position, Vector2 force) {
        body.applyForce(position, force);
    }

    public void applyAngularForce(float v) {
        body.applyAngularForce(v);
    }

    @Override
    public void onUpdate(float timeElapsed) {
        setPosition(body.getPosition());
        setRotation(body.getRotation());
    }

    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }
}
