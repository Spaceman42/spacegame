package com.x2a.graphics.j2d;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 40501 on 1/26/2015.
 */
public class J2dImageCache {
    private static Map<String, WeakReference<BufferedImage>> images = Collections.synchronizedMap(new HashMap<String, WeakReference<BufferedImage>>());


    private J2dImageCache() {}

    public static BufferedImage getImage(String location) {
        BufferedImage image = null;
        WeakReference<BufferedImage> ref = images.get(location);
        if (ref != null) {
            image = ref.get();
        }
        if (image == null) {
            try {
                image = ImageIO.read(new File(location));
                System.out.println("Image loaded: " + location);
                images.put(location, new WeakReference<BufferedImage>(image));
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

        return image;
    }
}
