package com.x2a.game;

/**
 * Created by Ethan on 2/2/2015.
 */
public class Animation {
    private int[] frames;
    private float[] frameTimes;

    private float totalTime;

    private float fullAnimationTime;

    public Animation(float[] frameTimes, int startFrame) {
        this.frameTimes = frameTimes;

        frames = new int[frameTimes.length];
        for (int i = 0; i<frames.length; i++) {
            frames[i] = i + startFrame;
        }
        fullAnimationTime = getAnimationTime();
    }

    public Animation(float[] frameTimes) {
        this(frameTimes, 0);
    }

    public Animation(int[] frames, float[] frameTimes) {
        this.frames = frames;
        this.frameTimes = frameTimes;
        fullAnimationTime = getAnimationTime();
    }

    private float getAnimationTime() {
        float total = 0;
        for (float f : frameTimes) {
            total += f;
        }
        return total;
    }


    public int updateAndGet(float dt) {
        return getFrameFromTimeChange(dt*1000);
    }

    public void setSubImage(int subImage) {
        totalTime = sumToInt(subImage);
    }

    private float sumToInt(int stop) {
        float sum = 0;
        for (int i = 0; i<stop; i++) {
            sum+=frameTimes[i];
        }
        return sum;
    }

    private int getFrameFromTimeChange(float dt) {
        totalTime += dt;
        if (totalTime > fullAnimationTime) {
            totalTime = 0;
        }

        return getFrameFromTime(totalTime);
    }

    public int getFrameFromTime(float time) {
        int i = 0;
        float sum = 0;
        for (i = 0; i <frameTimes.length; i++) {
            if (sum >= time) {
                return frames[i];
            }
            sum += frameTimes[i];

        }
        return 0;
    }


}
