package com.x2a.spacegame.prefab;

import com.x2a.game.PhysicsSimulator;
import com.x2a.game.Sprite;
import com.x2a.game.util.BasicControllerScript;
import com.x2a.game.util.RotateControllerScript;
import com.x2a.math.Vector2;
import com.x2a.physics.PhysicsWorld;
import com.x2a.physics.shape.RectangleShape;
import com.x2a.scene.GameObject;

import java.awt.geom.AffineTransform;

/**
 * Created by Ethan on 2/2/2015.
 */
public class SpaceShip extends GameObject{

    private Sprite sprite;

    private float turnRateMax;
    private float speedMax;

    private float accRate;
    private float turnAccRate;

    private float speed;
    private float turnRate;

    private boolean accelerating;

    public PhysicsSimulator ps;

    public SpaceShip(Sprite sprite, float speedMax, float turnRateMax, float accRate, float turnAccRate) {
        this.speedMax = speedMax;
        this.turnRateMax = turnRateMax;
        this.accRate = accRate;
        this.turnAccRate = turnAccRate;

        this.sprite = sprite;
        addChild(sprite);
        addChild(new RotateControllerScript('w', 'a', 's', 'd', 1000f, 0.1f));

    }

    public void turn(int dir) {
        if (ps != null) {
            if (dir > 0) {
                turnLeft();
            } else {
                turnRight();
            }
        }
    }

    @Override
    protected void onUpdate(float timeElapsed) {
        if (false) {
            ps = new PhysicsSimulator(new RectangleShape(sprite.getWidth(), sprite.getHeight()), 1f, 0.1f, 0.1f, getPosition(),
                    (PhysicsWorld)getScene().getWorld());
            addChild(ps);
        }
        //super.translate(new Vector2(speed * timeElapsed, 0));
        super.rotate((float) Math.toRadians(turnRate * timeElapsed));
        if (speed > 0) {
            speed -= 1f;
        } else if (speed < 0) {
            speed = 0;
        }
        if (turnRate > 0) {
            turnRate -= 0.15f;
        } else if (turnRate < 0) {
            turnRate += 0.15f;
        }

        if (speed == 0) {
            sprite.setSubImage(1);
        }
    }

    public void rotate(float r) {
        turn((int)Math.ceil(r));
    }

    public void trueTrans(Vector2 dir) {
        super.translate(dir);
    }

    public void trueRotate(float rot) {
        super.rotate(rot);
    }

    @Override
    public void translate(Vector2 dir) {
        //super.translate(dir);
        if (dir.x == 0 ^ dir.y == 0) {
            accelerate();
        }
    }

    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }


    public void accelerate() {
        speed += accRate;
        if (speed > speedMax) {
            speed = speedMax;
        }
        AffineTransform rt = new AffineTransform();
        rt.setToRotation(getRotation());
        if (ps != null) {
            ps.applyForce(new Vector2(getPosition()), new Vector2(2, 0).transform(rt));
        }
    }

    public void turnLeft() {
        turnRate += turnAccRate;
        if (Math.abs(turnRate) > turnRateMax) {
            turnRate = turnRateMax;
        }
        turnRate = turnRateMax;
        ps.applyAngularForce(0.1f);
    }

    public void turnRight() {
        turnRate -= turnAccRate;
        if (Math.abs(turnRate) > turnRateMax) {
            turnRate = -turnRateMax;
        }
        turnRate = -turnRateMax;
        ps.applyAngularForce(-0.1f);
    }

}
