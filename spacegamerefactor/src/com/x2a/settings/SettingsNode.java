package com.x2a.settings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 40501 on 1/23/2015.
 */
class SettingsNode {
    private Map[] maps;

    private static final int FLOAT_MAP = 0;
    private static final int INT_MAP = 1;
    private static final int BOOL_MAP = 2;
    private static final int STRING_MAP = 3;

    private String name;

    SettingsNode(String name) {
        this.name = name;
        maps = new Map[4];
    }

    private void putGeneric(int map, String key, Object value) {
        if (maps[map] == null) {
            maps[map] = new HashMap<String, Object>();
        }
        maps[map].put(key, value);
    }

    private Object getGeneric(int map, String key) {
        if (maps[map] != null) {
            return maps[map].get(key);
        }
        return null;
    }

    String writeNode() {
        StringBuilder output = new StringBuilder();
        Map<String, Float> floatMap = (Map<String, Float>)maps[FLOAT_MAP];
        if (floatMap != null) {
            for (String var : floatMap.keySet()) {
                float value = floatMap.get(var).floatValue();
                output.append(Settings.TYPE_FLOAT + var + " = " + "\"" + value + "\"" + "\n");
            }
        }

        Map<String, Integer> intMap = (Map<String, Integer>)maps[INT_MAP];
        if (intMap != null) {
            for (String var : intMap.keySet()) {
                int value = intMap.get(var);
                output.append(Settings.TYPE_INT + var + " = " + "\"" + value + "\"" + "\n");
            }
        }

        Map<String, Boolean> boolMap = (Map<String, Boolean>)maps[BOOL_MAP];
        if (boolMap != null) {
            for (String var : boolMap.keySet()) {
                boolean value = boolMap.get(var);
                output.append(Settings.TYPE_BOOLEAN + var + " = " + "\"" + value + "\"" + "\n");
            }
        }
        Map<String, String> stringMap = (Map<String, String>)maps[STRING_MAP];
        if (stringMap != null) {
            for (String var : stringMap.keySet()) {
                String value = stringMap.get(var);
                output.append(Settings.TYPE_STRING + var + " = " + "\"" + value + "\"" + "\n");
            }
        }
        return output.toString();
    }


    void putFloat(String key, float value) {
        putGeneric(FLOAT_MAP, key.toLowerCase(), value);
    }

    void putInt(String key, int value) {
        putGeneric(INT_MAP, key.toLowerCase(), value);
    }

    void putBoolean(String key, boolean value) {
        putGeneric(BOOL_MAP, key.toLowerCase(), value);
    }

    void putString(String key, String value) {
        putGeneric(STRING_MAP, key.toLowerCase(), value);
    }

    float getFloat(String key) {
        return ((Float)getGeneric(FLOAT_MAP, key.toLowerCase())).floatValue();
    }

    int getInt(String key) {
        return ((Integer)getGeneric(INT_MAP, key.toLowerCase())).intValue();
    }

    boolean getBoolean(String key) {
        return ((Boolean)getGeneric(BOOL_MAP, key.toLowerCase())).booleanValue();
    }

    String getString(String key) {
        return ((String)getGeneric(STRING_MAP, key.toLowerCase()));
    }

    public String getName() {
        return name;
    }
}
