package com.x2a.spacegame;

import com.x2a.game.*;
import com.x2a.input.KeyEventData;
import com.x2a.input.MouseEventData;
import com.x2a.scene.GameObject;
import com.x2a.scene.Scene;
import com.x2a.spacegame.menu.MenuScene;
import com.x2a.spacegame.prefab.SpaceShip;
import com.x2a.spacegame.prefab.sprites.SpaceshipSprite;

/**
 * Created by Ethan on 1/29/2015.
 */
public class SpaceGame implements Game{
    private SceneManager sceneManager;

    private Scene testScene;

    public SpaceGame() {
        sceneManager = new SceneManager();
    }

    @Override
    public void update(float timeElapsed) {
        sceneManager.getCurrentScene().update(timeElapsed);
    }

    @Override
    public GameObject getRootNode() {
        return sceneManager.getCurrentScene().getRootNode();
    }

    @Override
    public void onKeyEvent(KeyEventData data) {

    }

    @Override
    public void onMouseEvent(MouseEventData data) {

    }

    @Override
    public void initSceneGraph() {
        sceneManager.pushScene(new MenuScene());
        //ss.accelerate();
    }

    @Override
    public SceneManager getSceneManager() {
        return sceneManager;
    }
}
