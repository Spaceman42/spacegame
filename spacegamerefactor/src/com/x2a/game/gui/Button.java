package com.x2a.game.gui;

import com.x2a.App;
import com.x2a.game.GameUtil;
import com.x2a.game.ScaledSprite;
import com.x2a.game.Sprite;
import com.x2a.math.GameMath;
import com.x2a.math.Vector2;
import com.x2a.scene.GameObject;

import java.awt.geom.AffineTransform;

/**
 * Created by Ethan on 2/5/2015.
 */
public abstract class Button extends GameObject {

    private int[] listeningButtons;
    private boolean mouseOver;

    private ScaledSprite sprite;

    private int width;
    private int height;

    public Button(ScaledSprite sprite, int[] listeningButtons) {
        this.listeningButtons = listeningButtons;
        this.sprite = sprite;
        width = (int)sprite.getWidth();
        height = (int)sprite.getHeight();
        addChild(sprite);
    }

    public Button(ScaledSprite sprite) {
        this(sprite, new int[]{1});
    }


    @Override
    protected void onUpdate(float timeElapsed) {
        AffineTransform t = getAbsoluteTransform();

        if (GameUtil.pointInside(new Vector2(App.getMouseX(), App.getMouseY()), t, width, height)) {
            System.out.println("mouse over");
            mouseOver = true;
            for (int b : listeningButtons) {
                if (App.isButtonDown(b)) {
                    onClick(b);
                    break;
                }
            }
        } else {
            mouseOver = false;
        }
    }

    protected abstract void onClick(int button);

    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }

    public boolean isMouseOver() {
        return mouseOver;
    }

    public Sprite getSprite() {
        return sprite.getSprite();
    }
}
