package com.x2a.spacegame.menu;

import com.x2a.App;
import com.x2a.game.Sprite;
import com.x2a.scene.Scene;
import com.x2a.spacegame.prefab.SpaceShip;
import com.x2a.spacegame.prefab.sprites.SpaceshipSprite;

/**
 * Created by Ethan on 2/5/2015.
 */
public class MenuExitButton extends MenuButton{
    private static final String SPRITE_LOCATION = "res/images/menu/ExitButton.png";
    private static final int SPRITE_WIDTH = 800;
    private static final int SPRITE_HEIGHT = 240;

    public MenuExitButton(float yPosition) {
        super(new Sprite(SPRITE_LOCATION, 800, 240), new int[]{1}, yPosition);
    }

    @Override
    protected void onClick(int button) {
        if (button == 1) {
            App.endGame();
        }
    }
}
