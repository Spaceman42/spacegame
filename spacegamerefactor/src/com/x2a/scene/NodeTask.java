package com.x2a.scene;

/**
 * Created by Ethan on 1/26/2015.
 */
public interface NodeTask {
    public void doTask(GameObject n);
}
