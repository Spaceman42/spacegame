package com.x2a.game;

import com.x2a.App;
import com.x2a.graphics.render.Image;
import com.x2a.graphics.render.RenderAPI;
import com.x2a.scene.GameObject;

/**
 * Created by 40501 on 1/30/2015.
 */
public class Sprite extends GameObject {
    private Image image;
    private int subImage;

    public Sprite(Image image) {
        this.image = image;
    }

    public Sprite(String location, int regionWith, int regionHeight) {
        image = App.renderAPI().loadImage(location, regionWith, regionHeight);
        System.out.println(image);
    }

    @Override
    protected void onUpdate(float timeElapsed) {

    }

    @Override
    public void draw(RenderAPI renderAPI) {
        super.draw(renderAPI);
        renderAPI.drawImageRegion(getAbsoluteTransform(), subImage, image);
    }

    public int getSubImage() {
        return subImage;
    }

    public void setSubImage(int subImage) {
        this.subImage = subImage;
    }


    public Image getImage() {
        return image;
    }

    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }

    public int getWidth() {
        return image.getRegionWidth();
    }

    public int getHeight() {
        return image.getRegionHeight();
    }
}
