package com.x2a.engine;

import com.x2a.game.Game;
import com.x2a.graphics.render.Window;

/**
 * Created by Ethan on 1/29/2015.
 */
public class VariableGameLoop implements GameLoop{
    private static final long ONE_MILLISECOND = 1000000L;
    private static final float ONE_SECOND_MS = 1000.0f;

    private double targetTime;

    private long currentTime;
    private long lastFrameTime;

    private Game game;
    private Window window;

    private boolean running;

    private long totalTime;

    public VariableGameLoop(Game game, Window window, double targetTime) {
        this.game = game;
        this.window = window;
        this.targetTime = targetTime;

        Thread sleepThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(Long.MAX_VALUE);
                } catch (InterruptedException e) {}
            }
        });

        sleepThread.setName("X2A Game Timer Thread");
        sleepThread.setDaemon(true);
        sleepThread.start();
    }

    public void stop() {
        running = false;
    }

    public void run() {
        running = true;

        long lastTime = 0;

        while(running) {
            long currentTime = System.nanoTime();

            long dt = (Math.abs(lastTime-currentTime)/ONE_MILLISECOND);
            //System.out.println("DT: " + lastTime);
            lastTime = currentTime;

            doUpdate(dt);

            long finalTime = System.nanoTime();
            long finalDT = Math.abs(finalTime - currentTime);

            if (targetTime*ONE_MILLISECOND - finalDT > 0) {
                try {
                    //long preSleep = System.nanoTime();
                    Thread.sleep((long) ((targetTime*ONE_MILLISECOND - finalDT)/ONE_MILLISECOND));
                    //long postSleep = System.nanoTime();

                } catch (InterruptedException e) {}
            }
        }
        window.close();
        System.out.println("Game loop finished");
    }

    private void doUpdate(float dt) {
        game.update((float)dt/ONE_SECOND_MS);
        window.update(game.getRootNode());
    }
}
