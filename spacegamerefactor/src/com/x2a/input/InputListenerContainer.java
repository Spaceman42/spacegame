package com.x2a.input;

import com.x2a.scene.GameObject;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by 40501 on 1/29/2015.
 */
public class InputListenerContainer {
    private Set<WeakReference<GameObject>> nodes;

    public InputListenerContainer() {
        nodes = new HashSet<WeakReference<GameObject>>();
    }


    public void registerNode(GameObject n) {
        nodes.add(new WeakReference<GameObject>(n));
    }

    public Set<WeakReference<GameObject>> getNodes() {
        return nodes;
    }

    public GameObject getNode(WeakReference<GameObject> ref) {
        GameObject n = ref.get();
        return n;
    }

    public void onKeyEvent(KeyEventData data) {
        for (WeakReference<GameObject> ref : nodes) {
            GameObject n = ref.get();
            if (n != null) {
                n.doNodeTask((GameObject o) -> o.onKeyEvent(data));
            } else {
                nodes.remove(ref);
            }
        }
    }

    public void onMouseEvent(MouseEventData data) {
        for (WeakReference<GameObject> ref : nodes) {
            GameObject n = ref.get();
            if (n != null) {
                n.doNodeTask((GameObject o) -> o.onMouseEvent(data));
            } else {
                nodes.remove(ref);
            }
        }
    }
}
