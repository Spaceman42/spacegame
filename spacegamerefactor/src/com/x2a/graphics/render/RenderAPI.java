package com.x2a.graphics.render;

import com.x2a.math.Vector2;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Created by 40501 on 1/26/2015.
 */
public interface RenderAPI {
    public void drawImageRegion(AffineTransform transform, int region, Image image);
    public void drawRectangle(AffineTransform transform, Color color, boolean fill, int width, int height);
    public void drawOval(AffineTransform transform, Color color, boolean fill, int width, int height);
    public void drawLine(Color color, Vector2 p1, Vector2 p2);
    public Image loadImage(String location, int regionWidth, int regionHeight);
    public Image loadImage(int[] data, int width, int height, int regionWidth, int regionHeight);
    public Image loadImage(Color[] data, int width, int height, int regionWidth, int regionHeight);
}
