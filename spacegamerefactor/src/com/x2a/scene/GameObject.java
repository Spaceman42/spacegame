package com.x2a.scene;

import com.x2a.graphics.render.RenderAPI;
import com.x2a.input.KeyEventData;
import com.x2a.input.MouseEventData;
import com.x2a.math.Vector2;

import java.awt.geom.AffineTransform;
import java.util.*;

/**
 * Created by 40501 on 1/26/2015.
 */
public abstract class GameObject {
    private GameObject parent;
    private boolean isActivated = true;
    protected Collection<GameObject> children;

    private Vector2 position;
    private float rotation;
    private int depth;
    private float scaleX;
    private float scaleY;

    private AffineTransform transform;

    public GameObject() {
        this(new Vector2(), 0, 1, 0);
    }

    public GameObject(Vector2 position) {
        this(position, 0, 1, 0);
    }

    public GameObject(Vector2 position, int depth) {
        this(position, 0, 1, depth);
    }

    public GameObject(Vector2 position, float rotation, float scale, int depth) {
        super();
        this.position = new Vector2(position);
        this.rotation = rotation;
        this.depth = depth;
        this.scaleX = scale;
        this.scaleY = scale;
        createChildCollection();
    }

    protected void createChildCollection() {
        children = new ArrayList<GameObject>();
    }

    public GameObject getParent() {
        return parent;
    }

    public boolean removeChild(GameObject child) {
        child.parent = null;
        return children.remove(child);
    }

    /**
     * Allows any task to be performed by every node attached to this node
     * @param task code for all nodes associated with this node
     */
    public void doNodeTask(NodeTask task) {
        if (isActivated) {
            task.doTask(this);
            for (GameObject n : getChildren()) {
                task.doTask(n);
                n.doNodeTask(task);
            }
        }
    }

    public void update(float timeElapsed) {
        if (isActivated) {
            onUpdate(timeElapsed);
            for (GameObject n : getChildren()) {
                n.update(timeElapsed);
            }
        }
    }

    protected abstract void onUpdate(float timeElapsed);

    public void draw(RenderAPI renderAPI) {
    }

    public void addChild(GameObject child) {
        child.parent = this;
        children.add(child);
    }

    public void changeParent(GameObject newParent) {
        getParent().removeChild(this);
        newParent.addChild(this);
    }

    public Collection<GameObject> getChildren() {
        return children;
    }

    public Collection<GameObject> getAllNodes() {
        Set<GameObject> set = new HashSet<GameObject>();
        doNodeTask((GameObject n) -> set.add(n));
        return set;
    }

    public void onKeyEvent(KeyEventData data) {

    }

    public void onMouseEvent(MouseEventData data) {

    }

    public void activate() {
        doNodeTask((GameObject n) -> n.internalActivate());
    }

    public void deactivate() {
        doNodeTask((GameObject n) -> n.internalDeactivate());
    }

    private void internalActivate() {
        if (!isActivated) {
            onActivate();
            isActivated = true;
        }
    }
    protected void internalDeactivate() {
        if (isActivated) {
            onDeactivate();
            isActivated = false;
        }
    }

    protected abstract void onActivate();
    protected abstract void onDeactivate();

    public void setPosition(Vector2 position) {
        this.position = new Vector2(position);
    }

    public Vector2 getPosition() {
        return new Vector2(position);
    }

    public void translate(Vector2 translation) {
        Vector2 trans = new Vector2(translation);
        AffineTransform t = new AffineTransform();
        t.rotate(getRotation());
        trans.transform(t);
        setPosition(getPosition().add(trans));
    }

    public void transform(AffineTransform transform) {
        setPosition(getPosition().transform(transform));
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public void rotate(float theta) {
        setRotation(getRotation() + theta);
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public AffineTransform getTransform() {
        return transform;
    }

    /**
     * Creates a transform for the objects state at the time of calling. This is what getTransform returns
     */
    public void computeTransform() {
        AffineTransform t = new AffineTransform();

        t.translate(position.x, position.y);
        t.rotate(rotation);
        t.scale(scaleX, scaleY);

        transform = t;
    }

    public void setTransform(AffineTransform t) {
        transform = t;
    }

    public AffineTransform getAbsoluteTransform() {
        AffineTransform l = new AffineTransform();
        l.translate(position.x, position.y);
        l.rotate(rotation);
        l.scale(scaleX, scaleY);

        AffineTransform parentW = new AffineTransform(parent.getAbsoluteTransform());

        parentW.concatenate(l);
        return parentW;
    }

    public float getScaleX() {
        return scaleX;
    }

    public void setScale(float scale) {
        this.scaleX = scale;
        this.scaleY = scale;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    public void setScale(float scaleX, float scaleY) {
        setScaleX(scaleX);
        setScaleY(scaleY);
    }

    public float getScaleY() {
        return scaleY;
    }

    public Collection<GameObject> getAllOfType(Class clazz) {
        List<GameObject> output = new ArrayList<GameObject>();

        for (GameObject o : getChildren()) {
            if (clazz.isInstance(o)) {
                output.add(o);
            }
        }

        return output;
    }

    public Scene getScene() {
        return parent.getScene();
    }

    public void setPosition(float x, float y) {
        setPosition(new Vector2(x, y));
    }
}
