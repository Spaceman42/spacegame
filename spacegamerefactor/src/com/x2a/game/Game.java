package com.x2a.game;

import com.x2a.input.KeyEventData;
import com.x2a.input.MouseEventData;
import com.x2a.scene.GameObject;

/**
 * Created by Ethan on 1/26/2015.
 */
public interface Game {
    public void update(float timeElapsed);
    public GameObject getRootNode();

    public void onKeyEvent(KeyEventData data);
    public void onMouseEvent(MouseEventData data);

    public void initSceneGraph();

    public SceneManager getSceneManager();
}
