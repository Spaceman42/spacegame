package com.x2a.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 40501 on 1/23/2015.
 */
public class FileUtil {
    private FileUtil() {};

    public static String loadFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder out = new StringBuilder();
        try {
            String line;
            while ((line = br.readLine()) != null) {
                out.append(line);
            }
        } finally {
            br.close();
        }

        return out.toString();
    }

    public static List<String> loadFileLines(File file) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(file));
        List<String> out = new ArrayList<String>();
        try {
            String line;
            while ((line = br.readLine()) != null) {
                out.add(line);
            }
        } finally {
            br.close();
        }

        return out;
    }

    public static void writeFile(File file, String data) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        try {
           bw.write(data);
        } finally {
            bw.close();
        }
    }
}
