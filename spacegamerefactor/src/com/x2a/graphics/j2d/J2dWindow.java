package com.x2a.graphics.j2d;

import com.x2a.App;
import com.x2a.graphics.render.RenderAPI;
import com.x2a.graphics.render.Window;
import com.x2a.scene.GameObject;

import javax.swing.*;
import java.awt.*;

/**
 * Created by 40501 on 1/26/2015.
 */
public class J2dWindow implements Window{

    private DrawPanel drawPanel;
    private JFrame frame;

    private int xRes;
    private int yRes;
    private boolean fullScreen;

    Dimension screen;

    public J2dWindow() {
        xRes = App.settings().getInt("graphics", "xres");
        yRes = App.settings().getInt("graphics", "yres");
        fullScreen = App.settings().getBoolean("graphics", "fullscreen");

        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        screen = Toolkit.getDefaultToolkit().getScreenSize();

        if (fullScreen) {

            xRes = screen.width;
            yRes = screen.height;
            App.settings().setInt("graphics", "xres", xRes);
            App.settings().setInt("graphics", "yres", yRes);
            frame.setUndecorated(true);
        }

        drawPanel = new DrawPanel(xRes, yRes);
        frame.getContentPane().add(drawPanel);
    }

    @Override
    public RenderAPI getRenderAPI() {
        return drawPanel.getRenderAPI();
    }

    @Override
    public void update(GameObject rootNode) {
        drawPanel.update(rootNode);
    }

    @Override
    public void create() {
        frame.pack();

        frame.setVisible(true);
        frame.setLocation(screen.width/2-frame.getWidth()/2, screen.height/2-frame.getHeight()/2);

    }

    @Override
    public int getWidth() {
        return xRes;
    }

    @Override
    public int getHeight() {
        return yRes;
    }

    @Override
    public void close() {
        frame.dispose();
    }

    @Override
    public void mouseExited() {
        try {
            Robot r = new Robot();
            //r.mouseMove(frame.getX()+App.getRawMouseX(), frame.getY()+App.getRawMouseY());
        } catch (AWTException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public void mouseEntered() {

    }
}
