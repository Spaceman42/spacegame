package com.x2a.scene;

import com.x2a.math.Vector2;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;

/**
 * Created by Ethan on 1/26/2015.
 */
public class Camera extends GameObject {


    @Override
    protected void onUpdate(float timeElapsed) {

    }

    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }

    @Override
    public AffineTransform getAbsoluteTransform() {
        AffineTransform l = new AffineTransform();
        l.translate(getPosition().x, getPosition().y);
        l.rotate(getRotation());
        l.scale(getScaleX(), getScaleY());

        try {
            l = l.createInverse();
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }

        AffineTransform parentW = new AffineTransform(getParent().getAbsoluteTransform());

        parentW.concatenate(l);
        return parentW;
    }

}
