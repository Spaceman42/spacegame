package com.x2a.physics;

import org.jbox2d.dynamics.BodyType;

/**
 * Created by Ethan on 2/26/2015.
 */
public enum PhysicsBodyType {
    DYNAMIC(BodyType.DYNAMIC), STATIC(BodyType.STATIC), KINEMATIC(BodyType.KINEMATIC);

    private BodyType t;

    PhysicsBodyType(BodyType t) {
        this.t = t;
    }

    public BodyType getBodyType() {
        return t;
    }
}
