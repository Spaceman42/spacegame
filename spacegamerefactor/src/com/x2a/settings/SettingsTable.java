package com.x2a.settings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ethan on 1/25/2015.
 */
public class SettingsTable {
    Map<String, SettingsNode> settingsMap;

    public SettingsTable() {
        settingsMap = new HashMap<String, SettingsNode>();
    }

    public void putNode(SettingsNode node) {
        settingsMap.put(node.getName(), node);
    }

    Map<String, SettingsNode> getSettingsMap() {
        return settingsMap;
    }
}
