package com.x2a.spacegame.menu;

import com.x2a.App;
import com.x2a.game.PhysicsSimulator;
import com.x2a.game.Sprite;
import com.x2a.math.Vector2;
import com.x2a.physics.PhysicsWorld;
import com.x2a.physics.shape.RectangleShape;
import com.x2a.scene.Scene;
import com.x2a.scene.world.World;
import com.x2a.spacegame.prefab.SpaceShip;
import com.x2a.spacegame.prefab.sprites.SpaceshipSprite;

/**
 * Created by Ethan on 2/5/2015.
 */
public class MenuPlayButton extends MenuButton {
    private static final String SPRITE_LOCATION = "res/images/menu/PlayButton.png";
    private static final int SPRITE_WIDTH = 800;
    private static final int SPRITE_HEIGHT = 240;

    public MenuPlayButton(float yPosition) {
        super(new Sprite(SPRITE_LOCATION, 800, 240), new int[]{1}, yPosition);
    }

    @Override
    protected void onClick(int button) {
        Scene testScene = new Scene() {
            PhysicsWorld world = new PhysicsWorld(new Vector2(), 100);

            @Override
            public World getWorld() {
                return world;
            }

            @Override
            public void update(float dt) {
                super.update(dt);
                world.update(dt);
            }
        };
        SpaceShip spaceShip = new SpaceShip(new SpaceshipSprite(), 100, 20f, 10, 1f);
        spaceShip.addChild(new PhysicsSimulator(new RectangleShape(42, 30), 1f, 0.1f, 0.1f, spaceShip.getPosition(),
                (PhysicsWorld)testScene.getWorld()));
        testScene.getRootNode().addChild(spaceShip);
        SpaceShip ss = new SpaceShip(new SpaceshipSprite(), 100, 20f, 10, 1f);
        ss.setPosition(400, 0);
        testScene.getRootNode().addChild(ss);

        ss.ps = new PhysicsSimulator(new RectangleShape(42, 30), 1f, 0.1f, 0.1f, ss.getPosition(),
                (PhysicsWorld)testScene.getWorld());

        ss.addChild(ss.ps);
        App.game().getSceneManager().pushScene(testScene);
    }
}
