package com.x2a.engine;

/**
 * Created by Ethan on 1/29/2015.
 */
public interface GameLoop {

    public void stop();
    public void run();
}
