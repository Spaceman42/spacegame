package com.x2a.game;

import com.x2a.math.GameMath;
import com.x2a.math.Vector2;
import com.x2a.physics.shape.PhysicsShape;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;

/**
 * Created by 40501 on 3/3/2015.
 */
public final class GameUtil {
    private GameUtil() {}

    public static boolean pointInside(Vector2 p, AffineTransform t, float width, float height) {
        /*float x = (float)t.getTranslateX();
        float y = (float)t.getTranslateY();
        float scaleX = (float)t.getScaleX();
        float scaleY = (float)t.getScaleY();
        */
        AffineTransform it = t;
        try {
            it = t.createInverse();
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }
        Vector2 transP = p.transform(it);

        return (GameMath.pointInsideBounds(transP.x, transP.y, 0 - width/2.0f, 0 - height/2.0f, 0 + width/2.0f, 0 + height/2.0f));
    }

    public static boolean pointInside(Vector2 p, AffineTransform t, PhysicsShape shape) {
        return shape.testPoint(p, t);
    }
}
