package com.x2a.scene;

import com.x2a.scene.world.World;

import java.awt.geom.AffineTransform;

/**
 * Created by 40501 on 1/27/2015.
 */
public abstract class Scene {
    private GameObject rootNode;

    public Scene() {
        rootNode = new GameObject() {

            @Override
            public void computeTransform() {}

            @Override
            public AffineTransform getAbsoluteTransform() {
                return new AffineTransform();
            }

            @Override
            protected void onUpdate(float timeElapsed) {

            }

            @Override
            protected void onActivate() {

            }

            @Override
            protected void onDeactivate() {

            }

            @Override
            public Scene getScene() {
                return Scene.this;
            }

        };
    }

    public GameObject getRootNode() {
        return rootNode;
    }

    public void update(float timeElapsed) {
        rootNode.update(timeElapsed);
    }

    public void activate() {
        rootNode.activate();
    }

    public void deactivate() {
        rootNode.deactivate();
    }

    public abstract World getWorld();
}
