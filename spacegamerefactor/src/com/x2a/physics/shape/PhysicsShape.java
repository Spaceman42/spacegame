package com.x2a.physics.shape;

import com.x2a.math.Vector2;
import org.jbox2d.collision.shapes.Shape;

import java.awt.geom.AffineTransform;

/**
 * Created by Ethan on 2/26/2015.
 */
public interface PhysicsShape {
    public Shape getCollisionShape(int pixelsPerUnit);

    public boolean testPoint(Vector2 p, AffineTransform t);
}
