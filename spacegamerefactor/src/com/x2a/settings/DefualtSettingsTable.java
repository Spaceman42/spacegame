package com.x2a.settings;

/**
 * Created by Ethan on 1/25/2015.
 */
public class DefualtSettingsTable extends SettingsTable{

    public DefualtSettingsTable() {
        super();

        SettingsNode graphics = new SettingsNode("Graphics");
        graphics.putInt("Xres", 1280);
        graphics.putInt("Yres", 720);
        graphics.putBoolean("Fullscreen", false);

        putNode(graphics);
    }
}
