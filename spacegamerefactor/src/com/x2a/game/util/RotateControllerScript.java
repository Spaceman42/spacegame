package com.x2a.game.util;

import com.x2a.App;
import com.x2a.math.Vector2;
import com.x2a.scene.Script;

/**
 * Created by 40501 on 2/3/2015.
 */
public class RotateControllerScript extends Script{
    private char upKey;
    private char leftKey;
    private char downKey;
    private char rightKey;

    private float speed;
    private float rotateSpeed;

    public RotateControllerScript(char upKey, char leftKey, char downKey, char rightKey, float speed, float rotationSpeed) {
        this.upKey = upKey;
        this.leftKey = leftKey;
        this.downKey = downKey;
        this.rightKey = rightKey;

        this.speed = speed;
        this.rotateSpeed = rotationSpeed;
    }


    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }

    @Override
    public void onUpdate(float timeElapsed) {
        Vector2 translationVec = new Vector2();
        float transAmount = timeElapsed*speed;
        if (App.isKeyDown(upKey)) {
            translationVec.add(new Vector2(0, -transAmount));
        }
        if (App.isKeyDown(downKey)) {
            translationVec.add(new Vector2(0, transAmount));
        }
        if (App.isKeyDown(leftKey)) {
            rotate(-rotateSpeed);
        }
        if (App.isKeyDown(rightKey)) {
            rotate(rotateSpeed);
        }
        translate(translationVec);
    }
}
