package com.x2a.game;

import com.x2a.graphics.render.Image;

import java.util.Arrays;

/**
 * Created by Ethan on 1/30/2015.
 */
public class AnimatedSprite extends Sprite {

    private Animation[] animations;
    private int currentAnimation;

    public AnimatedSprite(Image image, Animation[] animations) {
        super(image);
        this.animations = animations;
    }

    public AnimatedSprite(String location, int regionWith, int regionHeight, Animation[] animations) {
        super(location, regionWith, regionHeight);
        this.animations = animations;
    }

    public AnimatedSprite(String location, int regionWith, int regionHeight, Animation animation) {
        super(location, regionWith, regionHeight);
        this.animations = new Animation[] {animation};
    }

    @Override
    public void onUpdate(float timeElapsed) {
        super.onUpdate(timeElapsed);
        super.setSubImage(animations[currentAnimation].updateAndGet(timeElapsed));
    }

    @Override
    public void setSubImage(int subImage) {
        animations[currentAnimation].setSubImage(subImage);
    }

    public void setAnimation(int animation) {
        if (animation < animations.length) {
            currentAnimation = animation;
        }
    }

    public int getCurrentAnimationIndex() {
        return currentAnimation;
    }

    public Animation getCurrentAnimation() {
        return animations[currentAnimation];
    }

}
