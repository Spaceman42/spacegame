package com.x2a.math.sat;

import com.x2a.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 12/28/2014.
 */
public class ConvexHull {

    public List<Vector2> points;
    public boolean failed = false;

    private ConvexHull(List<Vector2> outerPoints) {
        points = outerPoints;
    }

    public static ConvexHull createConvexHull(List<Vector2> points) {
        List<Vector2> convexPoints = new ArrayList<>();
        points = filterPoints(points);

        Vector2 averagePoint = averagePoint(points);
        Vector2 firstPoint = farthestPoint(points);
        float farthestLength = new Vector2(firstPoint).sub(averagePoint).mag();
        Vector2 nextPoint = ConvexHull.nextPoint(points, firstPoint, averagePoint, farthestLength);

        convexPoints.add(firstPoint);

        int i = 0;
        while (!firstPoint.equals(nextPoint) && i < 100) {
            convexPoints.add(nextPoint);
            nextPoint = ConvexHull.nextPoint(points, nextPoint, averagePoint, farthestLength);
            i++;
        }
        ConvexHull ch = new ConvexHull(convexPoints);
        if (i >= 100)
            ch.failed = true;
        return ch;
    }

    private static List<Vector2> filterPoints(List<Vector2> points) {
        for (int i = 0; i < points.size(); i++) {
            for (int j = 0; j < points.size(); j++) {
                if (i != j && points.get(i).equals(points.get(j))) {
                    points.remove(j);
                    if (i > j) {
                        i--;
                    }
                    j--;
                }
            }
        }
        return points;
    }

    public static Vector2 averagePoint(List<Vector2> points) {
        Vector2 accum = new Vector2();
        points.forEach((Vector2 v) -> accum.add(v));
        return accum.div(points.size());
    }

    public static Vector2 farthestPoint(List<Vector2> points) {
        Vector2 average = averagePoint(points);
        Vector2 farthest = average;
        for (Vector2 v : points) {
            if (new Vector2(average).sub(farthest).mag2() <
                    new Vector2(average).sub(v).mag2()) {
                farthest = v;
            }
        }
        return farthest;
    }

    public static float crossProductZ(Vector2 average, Vector2 current, Vector2 candidate, float farthestLength) {
        //current = new Vector2(current).unitVector().mult(farthestLength);
        Vector2 v1 = new Vector2(average).sub(current).unitVector();
        Vector2 v2 = new Vector2(candidate).sub(current).unitVector();

        float angleA = (float) Math.atan2(v1.y, v1.x);
        float angleB = (float) Math.atan2(v2.y, v2.x);
        System.out.println(Math.toDegrees(angleA) + ", " + Math.toDegrees(angleB));

        return (v1.x * v2.y) - (v1.y * v2.x);

//        return angleA - angleB;
    }

    public static float relativeDistance(Vector2 current, Vector2 candidate) {
        return new Vector2(candidate).sub(current).mag2();
    }

    public static Vector2 nextPoint(List<Vector2> points, Vector2 current, Vector2 average, float farthestLength) {
        float relDist = Float.NaN; // Hmm...
        float crossZ = -1;
        Vector2 candidate = null;
        for (Vector2 p : points) {
            float candidateCrossZ = crossProductZ(average, current, p, farthestLength);
            if (candidateCrossZ > crossZ) {
                candidate = p;
                crossZ = candidateCrossZ;
                relDist = relativeDistance(current, p);
            } else if (candidateCrossZ == crossZ) {
                if (relDist > relativeDistance(current, p)) {
                    candidate = p;
                    crossZ = candidateCrossZ;
                    relDist = relativeDistance(current, p);
                }
            }
        }
        return candidate;
    }
}
