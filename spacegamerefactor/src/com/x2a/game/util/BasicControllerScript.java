package com.x2a.game.util;

import com.x2a.App;
import com.x2a.math.Vector2;
import com.x2a.scene.Script;

/**
 * Created by Ethan on 2/1/2015.
 */
public class BasicControllerScript extends Script{
    private char upKey;
    private char leftKey;
    private char downKey;
    private char rightKey;

    private float speed;

    public BasicControllerScript(char upKey, char leftKey, char downKey, char rightKey, float speed) {
        this.upKey = upKey;
        this.leftKey = leftKey;
        this.downKey = downKey;
        this.rightKey = rightKey;

        this.speed = speed;
    }


    @Override
    protected void onActivate() {

    }

    @Override
    protected void onDeactivate() {

    }

    @Override
    public void onUpdate(float timeElapsed) {
        Vector2 translationVec = new Vector2();
        float transAmount = timeElapsed*speed;
        if (App.isKeyDown(upKey)) {
            translationVec.add(new Vector2(0, -transAmount));
        }
        if (App.isKeyDown(downKey)) {
            translationVec.add(new Vector2(0, transAmount));
        }
        if (App.isKeyDown(leftKey)) {
            translationVec.add(new Vector2(-transAmount, 0));
        }
        if (App.isKeyDown(rightKey)) {
            translationVec.add(new Vector2(transAmount, 0));
        }
        translate(translationVec);
    }
}
