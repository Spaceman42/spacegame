package com.x2a.physics;

import com.x2a.math.Vector2;
import org.jbox2d.dynamics.World;

/**
 * Created by Ethan on 2/26/2015.
 */
public class PhysicsWorld implements com.x2a.scene.world.World{
    private static final int VELOCITY_ITERATIONS = 6;
    private static final int POSITION_ITERATIONS = 3;

    private World world;

    private final int pixelsPerUnit;

    public PhysicsWorld(Vector2 gravity, int pixelsPerUnit) {
        this.pixelsPerUnit = pixelsPerUnit;

        world = new World(MathUtil.toVec2(gravity));
        world.setAllowSleep(true);
    }

    @Override
    public void update(float timeElapsed) {
        world.step(timeElapsed, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
    }

    public int getPixelsPerUnit() {
        return pixelsPerUnit;
    }

    protected World getWorld() {
        return world;
    }
}
